#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.model.dao_list_sqlite import Dao_list_sqlite as List_db
from src.model.dao_todo_sqlite import Dao_todo_sqlite as Todo_db

class Form():
    def __init__(self):
        self.l_db = List_db()
        self.t_db = Todo_db()

    def n_list_reto(self, reto):
        return self.l_db.save(reto)

    def e_list_reto(self, reto):
        return self.l_db.modify(reto)

    def d_list_reto(self, id):
        return self.l_db.destroy_by_id(id)

    def n_todo(self, todo):
        return self.t_db.save(todo)

    def e_todo(self, todo):
        return self.t_db.modify(todo)

    def d_todo(self, id):
        return self.t_db.destroy_by_id(id)
