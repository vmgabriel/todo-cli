#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.model.dao_list_sqlite import Dao_list_sqlite as Db_sqlite_list
from src.model.dao_todo_sqlite import Dao_todo_sqlite as Db_sqlite_todo

from src.model.list import List_reto
from src.model.todo import Todo

from src.model.reto_todo import Reto_todo

class List_out():
    def __init__(self):
        self.r_t = Reto_todo()
        self.db_list = Db_sqlite_list()
        self.db_todo = Db_sqlite_todo()

    def print_list_reto_not_archived(self):
        self.db_list.print_l_list(self.db_list.l_to_List_reto(self.r_t.find_by_reto_not_archived_completed()))

    def print_list_reto_archived(self):
        self.db_list.print_l_list(self.db_list.l_to_List_reto(self.r_t.find_by_reto_completed()))

    def print_todo_of_reto(self, reto):
        self.db_todo.print_l_todo(self.db_todo.l_to_todo(self.db_todo.find_by_Reto(reto)))

    def id_list_reto(self, id):
        return self.db_list.l_to_List_reto_especific(self.db_list.find_by_id(id))[0]

    def id_todo(self, id):
        return self.db_todo.l_to_todo(self.db_todo.find_by_id(id))[0]

    def id_list_reto_completed(self, id):
        return self.db_list.l_to_List_reto(self.r_t.find_by_reto_completed_with_id(id))[0]
