#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.model.dao_list_sqlite import Dao_list_sqlite as List_sqlite
from src.model.dao_todo_sqlite import Dao_todo_sqlite as Todo_sqlite

class N_db():
    def __init__(self):
        self.list = List_sqlite()
        self.todo = Todo_sqlite()

    def execute_ddl(self):
        a = self.list.ddl()
        b = self.todo.ddl()

        return a or b
