#!/usr/bin/env python
# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod
from src.model.model import Model
#TODO: Agregar comentarios para documentacion

# Esto es una virtualizacion de una interfaz ya que python no necesita este

class Dao_crud(ABC):
    def __init__(self, _modelo):
        super().__init__()
        self.modelo = _modelo

    @abstractmethod
    def save(self, list_reto):
        pass

    @abstractmethod
    def find(self, data="", lvar=[]):
        pass

    @abstractmethod
    def modify_many(self, data, lvar):
        pass

    @abstractmethod
    def destroy(self, data, lvar):
        pass
