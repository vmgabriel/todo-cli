#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append("..")

from src.config.db import Db

from src.model.list import List_reto
from src.model.todo import Todo

class Read_db(object):
    def __init__(self):
        self.db = Db()

    def find(self, data, lvar, params):
        query = "SELECT {} FROM {};".format(params, data)
        return self.db.read_query(query, lvar)
