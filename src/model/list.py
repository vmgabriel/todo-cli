#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.model.model import Model

#TODO: Agregar comentarios para documentacion

class List_reto(Model):
    def __init__(self, _id=0, _nombre="", _descripcion="",
                 _estado=0, _porcentaje=0.1):
        super().__init__()

        self.name_table = "lista"
        self.name_var = ["id_reto",
                         "nombre",
                         "descripcion",
                         "estado"]

        self.id_reto = _id
        self.nombre = _nombre
        self.descripcion = _descripcion
        self.estado = _estado
        self.porcentaje = _porcentaje

    def get_tuple_var_with_id(self):
        return [self.id_reto, self.nombre, self.descripcion, self.estado]

    def get_tuple_var_idReverse(self):
        return (self.get_tuple_var_without_id() + [self.id_reto])

    def get_name_var_estado(self):
        return self.name_var[-1]

    def __str__(self):
        out = "%d. - [" % self.id_reto
        if (self.estado == 0):
            out += " "
        elif (self.estado == 1 or self.porcentaje == 100):
            out += "X"
        elif (self.estado == 2):
            out += "A"
        out += "] - "
        out += "{} - ".format(self.nombre)
        if (self.porcentaje == None):
            out += "Nothing Todo\n"
        else:
            out += "{}% Completed\n".format(self.porcentaje)
        out += "    %s" % self.descripcion
        return out
