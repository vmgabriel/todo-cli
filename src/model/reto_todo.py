#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.model.list import List_reto
from src.model.todo import Todo

from src.model.read_db import Read_db

class Reto_todo(object):
    def __init__(self, _reto=List_reto()):
        self.reto = _reto
        self.r_db = Read_db()

    def find_by_reto_completed(self, archived=""):
        tab_1 = Todo().name_table
        tab_2 = self.reto.name_table
        query = tab_1 + ", " + tab_2
        tab_3 = "{}.{} = {}.{}".format(tab_2, self.reto.get_id_var(),
                                       tab_1,Todo().get_fk_name())
        tab_2 = "{}.{} = 1".format(tab_1, Todo().get_completo_name())
        query = "SELECT COUNT(*) FROM {} WHERE {} AND {}".format(query,
                                                                 tab_3, tab_2)
        query1 = "SELECT COUNT(*)*0.01 FROM {} WHERE {}".format(tab_1, tab_3)
        query = "({})/({})".format(query, query1)
        query = "*, ROUND({})".format(query)

        if (archived == ""):
            return self.r_db.find(self.reto.name_table, [], query)
        else:
            data = self.reto.name_table
            data += " WHERE {}".format(archived)
            return self.r_db.find(data, [], query)

    def find_by_reto_not_archived_completed(self):
        data = "{}.{} != 2".format(self.reto.name_table,
                                  self.reto.get_name_var_estado())
        return self.find_by_reto_completed(data)

    def find_by_reto_completed_with_id(self, id):
        data = "{}.{} = {}".format(self.reto.name_table,
                                   self.reto.get_id_var(),id)
        return self.find_by_reto_completed(data)
