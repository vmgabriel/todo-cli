#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("..")

from src.config.db import Db

from src.model.model import Model
from src.model.dao_crud import Dao_crud
from src.model.read_db import Read_db

#TODO: Agregar comentarios para documentacion

class Dao_crud_sqlite(Dao_crud):
    def __init__(self, _modelo):
        super().__init__(_modelo)

        self.db = Db()

    def save(self, list_reto):
        query = "INSERT INTO {}({}) VALUES ({});".format(self.modelo.name_table,
                                                         self.modelo.get_vars_without_rowid(),
                                                         self.modelo.get_interrogate_var_noid())
        return self.db.write_query(query, list_reto.get_tuple_var_without_id())

    def find(self, data="", lvar=[]):
        r_db = Read_db()
        if (data == ""):
            return r_db.find(self.modelo.name_table, lvar, "*")
        else:
            data = self.modelo.name_table + " WHERE " + data
            return r_db.find(data, lvar, "*")

    def modify_many(self, data, lvar):
        query = "UPDATE {} SET {} WHERE {};".format(self.modelo.name_table,
                                                    self.modelo.get_vars_update(),
                                                    data)
        return self.db.write_query(query, lvar)

    def destroy(self, data, lvar):
        query = "DELETE FROM {} WHERE {};".format(self.modelo.name_table,
                                                  data)
        return self.db.write_query(query, lvar)
