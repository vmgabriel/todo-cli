#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import reduce

#TODO: Agregar comentarios para documentacion

class Model(object):
    def __init__(self):
        self.name_table = ""
        self.name_var = []

    def get_id_var(self):
        return self.name_var[0]

    def get_tuple_var_with_id(self):
        return ["1", "not value"]

    def get_tuple_var_idReverse(self):
        return []

    def get_interrogate_var(self):
        _temp = list(map(lambda x: "?", self.name_var))
        return reduce((lambda x, y: x+","+y), _temp)

    def get_interrogate_var_noid(self):
        _temp = list(map(lambda x: "?", self.name_var[1:]))
        return reduce((lambda x, y: x+","+y), _temp)

    def get_tuple_var_without_id(self):
        return self.get_tuple_var_with_id()[1:]

    def get_vars_with_rowid(self):
        return reduce((lambda x, y: x+","+y), self.name_var)

    def get_vars_without_rowid(self):
        return reduce((lambda x, y: x+","+y), self.name_var[1:])

    def get_vars_update(self):
        return (reduce((lambda x,y: x+" = ?, "+y), self.name_var[1:])+" = ?")

    def get_id_interrogate(self):
        return (self.name_var[0]+" = ?")
