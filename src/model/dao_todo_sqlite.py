#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.model.todo import Todo
from src.model.dao_crud_sqlite import Dao_crud_sqlite as Dao_sqlite

#TODO: Agregar comentarios para documentacion

class Dao_todo_sqlite(Dao_sqlite):
    def __init__(self):
        super().__init__(Todo())

    def find_by_id(self, id):
        return self.find(self.modelo.get_id_interrogate(), [id])

    def find_by_Reto(self, _reto):
        search = "fk_reto = ?"
        return self.find(search, [_reto.id_reto])

    def modify(self, model):
        lvar = model.get_tuple_var_idReverse()
        return self.modify_many(self.modelo.get_id_interrogate(), lvar)

    def destroy_by_id(self, id):
        return self.destroy(self.modelo.get_id_interrogate(), [id])

    def l_to_todo(self, _l):
        return list(map((lambda x: Todo(x[0], x[1], x[2], x[3], x[4])),_l))

    def print_l_todo(self, _l):
        if (len(_l) == 0):
            print ("There is not Elements for todo")
        else:
            for i in _l:
                print (str(i))

    def ddl(self):
        query = """CREATE TABLE IF NOT EXISTS todo
            (
              id_todo INTEGER PRIMARY KEY,
              nombre CHARACTER(40) NOT NULL,
              descripcion CHARACTER(300),
              completo INTEGER(1) NOT NULL,
              fk_reto INTEGER NOT NULL,
              FOREIGN KEY(fk_reto) REFERENCES lista(id_reto)
            );"""
        return self.db.write_query(query)
