#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.model.list import List_reto
from src.model.dao_crud_sqlite import Dao_crud_sqlite as Dao_sqlite

#TODO: Agregar comentarios para documentacion

class Dao_list_sqlite(Dao_sqlite):
    def __init__(self):
        super().__init__(List_reto())

    def find_by_id(self, id):
        return self.find(self.modelo.get_id_interrogate(), [id])

    def modify(self, modelo):
        data = modelo.get_id_interrogate()
        lvar = modelo.get_tuple_var_idReverse()
        return self.modify_many(data, lvar)

    def destroy_by_id(self, id):
        return self.destroy(self.modelo.get_id_interrogate(), [id])

    def l_to_List_reto(self, _l):
        return list(map((lambda x: List_reto(x[0],x[1],x[2],x[3],x[4])), _l))

    def l_to_List_reto_especific(self, _l):
        return list(map((lambda x: List_reto(x[0],x[1],x[2],x[3])), _l))

    def print_l_list(self, _l):
        if (len(_l) == 0):
            print ("There is not challenges")
        else:
            for i in _l:
                print (str(i))

    def ddl(self):
        query = """CREATE TABLE IF NOT EXISTS lista
            (
              id_reto INTEGER PRIMARY KEY,
              nombre CHARACTER(40) NOT NULL,
              descripcion CHARACTER(300),
              estado INTEGER(1) NOT NULL
            );"""
        return self.db.write_query(query)
