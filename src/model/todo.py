#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.model.model import Model

#TODO; Agregar Comentarios para Documentacion

class Todo(Model):
    def __init__(self, _id = 0, _nombre="", _descripcion="", _completo=False, _fk_reto=0):
        super().__init__()

        self.name_table = "todo"
        self.name_var = ["id_todo",
                         "nombre",
                         "descripcion",
                         "completo",
                         "fk_reto"]

        self.id_todo = _id
        self.nombre = _nombre
        self.descripcion = _descripcion
        self.completo = _completo
        self.fk_reto = _fk_reto

    def get_tuple_var_with_id(self):
        return [self.id_todo, self.nombre, self.descripcion,
                self.completo, self.fk_reto]

    def get_tuple_var_idReverse(self):
        return (self.get_tuple_var_without_id() + [self.id_todo])

    def get_completo_name(self):
        return self.name_var[-2]

    def get_fk_name(self):
        return self.name_var[-1]

    def __str__(self):
        out = "    {}. - [".format(self.id_todo)
        if (self.completo == 1):
            out += "X"
        else:
            out += " "
        out += "] - {}\n      {}".format(self.nombre, self.descripcion)
        return out
