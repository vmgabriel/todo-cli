#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

#TODO: Complete the documentacion in method of class Db -> Conexion
#TODO: Complete isolate configuration

class Db(object):
    def __init__(self):
        self.bd = "db/datos"
        self.info = {"changes": 0, "data": []}

    def write_query(self, query, value = []):
        _temp = 0
        self.init_db()

        self.query_val(query, value)
        self.con.commit()

        _temp = self.con.total_changes

        self.close_db()
        return _temp

    def read_query(self, query, value = []):
        _temp = []
        self.init_db()

        self.query_val(query, value)

        _temp = self.cursor.fetchall()

        self.close_db()
        return _temp

    def query_val(self, query, value = []):
        if (value == []):
            self.cursor.execute(query)
        else:
            self.cursor.execute(query, value)

    def init_db(self):
        self.con = sqlite3.connect(self.bd)
        self.cursor = self.con.cursor()

    def close_db(self):
        self.con.close()

    def message(self):
        print("Mensaje de la clase de la Base de Datos")
