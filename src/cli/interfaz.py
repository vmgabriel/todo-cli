#!/usr/bin/env python
# -*- coding: utf-8 -*-

import functools
import re

class Interfaz():
    def __init__(self):
        pass

    def limit(self):
        print ("---")

    def load_db(self):
        print ("Load DB")
        self.limit()

    def load_data(self):
        print ("Load Info In Database")
        self.limit()

    def create_path(self):
        print ("The folder does not exist, creating")
        self.limit()

    def config_db(self):
        print ("Configuring DB")
        self.limit()

    def success(self):
        print ("Done Correctly")
        self.limit()

    def print_List_Lista_reto(self):
        print (" id. - [state] - name  - percent completed - description")
        self.limit()

    def print_List_Todo(self):
        print (" id. -[state] - name - description")
        self.limit()

    def create_Db(self):
        print ("Create DB")
        self.limit()

    def error_args(self, args):
        args = functools.reduce((lambda x,y: x+","+y), args)
        print ("The arguments " +args+ " No Exist")
        self.limit()

    def error_db(self):
        print ("Error Conecction DB")
        self.limit()

    def error_data_value(self, val, typ):
        print ("Error "+val+" is not "+typ+".")

    def error_value_not_defined(self, val):
        print ("Error "+val+" is not defined")

    def not_exist_list_reto(self, id):
        print ("Not exist challenge with id={}".format(id))

    def not_exist_todo(self, id):
        print ("Not exist Todo with id={}".format(id))

    def option_not_defined(self):
        print("Option not defined.")

    def confirme_delete(self):
        inp = input("Do you are sure?(y|n): ")
        if (re.search(r'^.*y.*$', inp)):
            return 0
        if (re.search(r'^.*n.*$', inp)):
            return 1
        return 2

    def version(self):
        print ("challenge v0.3 (c) 2018 by Gabriel Vargas Monroy")

    def help(self):
        h = "usage: challenge [-banerdtsmocuv][--version][--help]\n"
        h += "  ----- Listing Options -----\n"
        h += "  -b build              Build or re-build path and DB.\n"
        h += "  -a all                All challenges are listed.\n"
        h += "  -n new                New challenge for save.\n"
        h += "  -e edit [id]          Edit a challenge with defined id.\n"
        h += "  -r archived [id]      Archived challenge with id defined.\n"
        h += "  -v unarchived [id]    Unarchived challenge with id defined.\n"
        h += "  -d delete [id]        Delete a challenge with defined id.\n"
        h += "  -t todo [id]          List of Todo elements of challenge with id defined.\n"
        h += "  -s save [id]          Save todo with id defined.\n"
        h += "  -m modify [id_todo]   Edit a Todo with id defined.\n"
        h += "  -o destroy [id_todo]  Delete a todo with id defined.\n"
        h += "  -c complete [id]      Complete todo with id defined.\n"
        h += "  -u uncomplete [id]    Uncomplete todo with id defined.\n"
        h += "  ----- Miscellaneous Options -----\n"
        h += "  --version version     Print version and exit.\n"
        h += "  --help help           Pirnt usage andthis help message and exit."
        print (h)
