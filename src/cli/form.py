#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

from src.cli.interfaz import Interfaz

from src.model.list import List_reto
from src.model.todo import Todo

class Form_ux():
    def __init__(self):
        self.interfaz = Interfaz()

    def n_reto(self):
        print("  -- New Challenge -- ")
        self.interfaz.limit()

        name = input("  Name: ")
        description = input("  Description: ")
        self.interfaz.limit()

        if(len(name) > 40):
            print("  Error: Name is too long - max 40 characters")
        elif(len(description) > 300):
            print("  Error: Description is too long - max 300 characters")
        elif(name == "" or re.search(r'/S*', name)):
            print("  Error: Descripion not defined")
        elif(description == "" or re.search(r'/S*', description)):
            print("  Error: Description not defined")
        else:
            l_reto = List_reto()

            l_reto.nombre = name
            l_reto.descripcion = description

            return l_reto
        return 1

    def e_reto(self, reto):
        l_reto = List_reto()
        l_reto.id_reto = reto.id_reto
        l_reto.estado = reto.estado

        print("  -- Edit Challenge -- ")
        self.interfaz.limit()

        name = input("  Name(default={}): ".format(reto.nombre))
        description = input("  Description(default={}): ".format(reto.descripcion))
        self.interfaz.limit()

        if(len(name) > 40):
            print("  Error: Name is too long - max 40 characters")
            return 1
        elif(len(description) > 300):
            print("  Error: Description is too long - max 300 characters")
            return 1
        elif(name == "" or re.search(r'/S*', name)):
            l_reto.nombre = reto.nombre
        elif(description == "" or re.search(r'/S*', description)):
            l_reto.descripcion = reto.descripcion
        else:
            l_reto.nombre = name
            l_reto.descripcion = description
        return l_reto

    def n_todo(self):
        print("  -- New Todo -- ")
        self.interfaz.limit()

        name = input("  Name: ")
        description = input("  Description: ")
        self.interfaz.limit()

        if(len(name) > 40):
            print("  Error: Name is too long - max 40 characters")
        elif(len(description) > 300):
            print("  Error: Description is too long - max 300 characters")
        elif(name == "" or re.search(r'/S*', name)):
            print("  Error: Descripion not defined")
        elif(description == "" or re.search(r'/S*', description)):
            print("  Error: Description not defined")
        else:
            n_todo = Todo()

            n_todo.nombre = name
            n_todo.descripcion = description

            return n_todo
        return 1

    def e_todo(self, todo):
        print("  -- Edit Todo -- ")
        self.interfaz.limit()

        name = input("  Name({}): ".format(todo.nombre))
        description = input("  Description({}): ".format(todo.descripcion))
        self.interfaz.limit()

        if(len(name) > 40):
            print("  Error: Name is too long - max 40 characters")
            return 1
        elif(len(description) > 300):
            print("  Error: Description is too long - max 300 characters")
            return 1
        else:
            if(not name == "" or not re.search(r'/S*', name)):
                todo.nombre = name
            if(not description == "" or not re.search(r'/S*', description)):
                todo.descripcion = description
            return todo
