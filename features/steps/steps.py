#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess

from behave import *

@given('challenge install process')
def step_impl(context):
    context.main = "./challenges.py"

@given('buid command defined')
def step_impl(context):
    context.command = context.main + " build"

@when('execute build challenge')
def step_impl(context):
    context.output = os.popen(context.command).read()

@when('the command return Correctly process')
def step_impl(context):
    correct_out = "Load DB\n---\nDone Correctly\n---Load Info In Database\n---\n\n"
    context.output == correct_out
