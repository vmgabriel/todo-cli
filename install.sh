#!/bin/bash

app="/usr/share/challenges"
src="$app/src"
db_cfg="$src/config"
main="$app/challenges.py"
db="~/.config/challenges"

echo "Installing script"

echo "Creating path APP"
mkdir -p $db
sudo mkdir -p $src
sudo cp -R ./src/* $src
sudo cp ./challenges.desktop $app
sudo mv "$app/challenges.desktop" $main

echo "Configuring script"
sudo cp -Rf ./config/* $db_cfg

echo "Send Script"
sudo ln -s $main challenges
sudo mv challenges /usr/bin
sudo chmod 755 $main

echo "Executing build"
challenges build
