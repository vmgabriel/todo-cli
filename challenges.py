#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import os

from src.config.init_path import Path_db

from src.process.n_db import N_db
from src.process.form import Form
from src.process.list import List_out

from src.cli.form import Form_ux
from src.cli.interfaz import Interfaz

interfaz = Interfaz()
path = Path_db()
l_out = List_out()
f_ux = Form_ux()
form = Form()

def print_not_all():
    interfaz.print_List_Lista_reto()
    l_out.print_list_reto_not_archived()

def print_all():
    interfaz.print_List_Lista_reto()
    l_out.print_list_reto_archived()

def error_args():
    interfaz.error_args(sys.argv[1:])
    help()

def help():
    interfaz.help()

def version():
    interfaz.version()

def build():
    if (not path.exist_path()):
        interfaz.create_path()
        path.init_path()
    else:
        interfaz.load_db()
        interfaz.success()
    interfaz.load_data()

    if (N_db().execute_ddl() != 0):
        interfaz.config_db()
        interfaz.success()

def help():
    interfaz.help()

def new():
    n = f_ux.n_reto()
    if (n != 1):
        if (form.n_list_reto(n) == 1):
            interfaz.success()
        else:
            interfaz.error_db()

def edit(id):
    if (re.search(r'\d+', id)):
        obj = l_out.id_list_reto(int(id))
        if (obj):
            obj = f_ux.e_reto(obj)
            if (not obj == 1):
                if (form.e_list_reto(obj) == 1):
                    interfaz.success()
                else:
                    interfaz.error_db()
        else:
            interfaz.not_exist_list_reto(id)
    else:
        interfaz.error_data_value(id,"integer")

def destroy(id):
    if (re.search(r'\d+', id)):
        if (interfaz.confirme_delete() == 0):
            if (form.d_list_reto(id) == 1):
                interfaz.success()
            else:
                interfaz.error_db()
        elif (interfaz.confirme_delete() == 2):
            interfaz.option_not_defined()
    else:
        interfaz.error_data_value(id,"integer")

def archived(id):
    if (re.search(r'\d+', id)):
        obj = l_out.id_list_reto(int(id))
        if (obj):
            if (not obj == 1):
                obj.estado = 2
                if (form.e_list_reto(obj) == 1):
                    interfaz.success()
                else:
                    interfaz.error_db()
        else:
            interfaz.not_exist_list_reto(id)
    else:
        interfaz.error_data_value(id,"integer")

def todo_reto(id):
    if (re.search(r'\d+', id)):
        obj = l_out.id_list_reto_completed(int(id))
        if (obj):
            print (obj)
            interfaz.limit()
            interfaz.print_List_Todo()
            l_out.print_todo_of_reto(obj)
        else:
            interfaz.not_exist_list_reto(id)
    else:
        interfaz.error_data_value(id,"integer")

def save_todo(id):
    if (re.search(r'\d+', id)):
        obj = l_out.id_list_reto(int(id))
        if (not obj == None):
            n = f_ux.n_todo()
            if (n != 1):
                n.fk_reto = int(id)
                if (form.n_todo(n)  == 1):
                    interfaz.success()
                else:
                    interfaz.error_db()
        else:
            interfaz.not_exist_list_reto(id)
    else:
        interfaz.error_data_value(id,"integer")

def modify(id):
    if (re.search(r'\d+', id)):
        obj = l_out.id_todo(int(id))
        if (obj):
            obj = f_ux.e_todo(obj)
            if (not obj == 1):
                if (form.e_todo(obj) == 1):
                    interfaz.success()
                else:
                    interfaz.error_db()
        else:
            interfaz.not_exist_todo(id)
    else:
        interfaz.error_data_value(id,"integer")

def delete(id):
    if (re.search(r'\d+', id)):
        if (interfaz.confirme_delete() == 0):
            if (form.d_todo(int(id)) == 1):
                interfaz.success()
            else:
                interfaz.error_db()
        elif (interfaz.confirme_delete() == 2):
            interfaz.option_not_defined()
    else:
        interfaz.error_data_value(id,"integer")

def complete(id):
    if (re.search(r'\d+', id)):
        obj = l_out.id_todo(int(id))
        if (obj):
            obj.completo = 1
            if (form.e_todo(obj) == 1):
                interfaz.success()
            else:
                interfaz.error_db()
        else:
            interfaz.not_exist_todo(id)
    else:
        interfaz.error_data_value(id,"integer")

def unarchived(id):
    if (re.search(r'\d+', id)):
        obj = l_out.id_list_reto(int(id))
        if (obj):
            if (not obj == 1):
                obj.estado = 0
                if (form.e_list_reto(obj) == 1):
                    interfaz.success()
                else:
                    interfaz.error_db()
        else:
            interfaz.not_exist_list_reto(id)
    else:
        interfaz.error_data_value(id,"integer")

def uncomplete(id):
    if (re.search(r'\d+', id)):
        obj = l_out.id_todo(int(id))
        if (obj):
            obj.completo = 0
            if (form.e_todo(obj) == 1):
                interfaz.success()
            else:
                interfaz.error_db()
        else:
            interfaz.not_exist_todo(id)
    else:
        interfaz.error_data_value(id,"integer")

if (len(sys.argv) == 1):
    print_not_all()
elif (len(sys.argv) >= 2):
    if (sys.argv[1] == "--help" or sys.argv[1] == "help"):
        help()
    elif (sys.argv[1] == "--version" or sys.argv[1] == "version"):
        version()
    elif (re.search(r'^-[banerdtsmocuv]+$', sys.argv[1])):
        if (re.search(r'^-.*b.*$', sys.argv[1])):
            build()
        if (re.search(r'^-.*a.*$', sys.argv[1])):
            print_all()
        if(re.search(r'^-.*n.*$', sys.argv[1])):
            new()
        if(re.search(r'^-.*e.*$', sys.argv[1])):
            if (len(sys.argv) == 3):
                edit(sys.argv[2])
            else:
                interfaz.error_args("id")
        if(re.search(r'^-.*r.*$', sys.argv[1])):
            if (len(sys.argv) == 3):
                archived(sys.argv[2])
            else:
                interfaz.error_args("id")
        if(re.search(r'^-.*v.*$', sys.argv[1])):
            if (len(sys.argv) == 3):
                unarchived(sys.argv[2])
            else:
                interfaz.error_args("id")
        if(re.search(r'^-.*d.*$', sys.argv[1])):
            if (len(sys.argv) == 3):
                destroy(sys.argv[2])
            else:
                interfaz.error_args("id")
        if(re.search(r'^-.*t.*$', sys.argv[1])):
            if (len(sys.argv) == 3):
                destroy(sys.argv[2])
            else:
                interfaz.error_value_not_defined("id")
                interfaz.limit()
                help()
        if(re.search(r'^-.*s.*$', sys.argv[1])):
            if (len(sys.argv) == 3):
                save_todo(sys.argv[2])
            else:
                interfaz.error_value_not_defined("id")
                interfaz.limit()
                help()
        if(re.search(r'^-.*m.*$', sys.argv[1])):
            if (len(sys.argv) == 3):
                modify(sys.argv[2])
            else:
                interfaz.error_value_not_defined("id")
                interfaz.limit()
                help()
        if(re.search(r'^-.*o.*$', sys.argv[1])):
            if (len(sys.argv) == 3):
                delete(sys.argv[2])
            else:
                interfaz.error_value_not_defined("id")
                interfaz.limit()
                help()
        if(re.search(r'^-.*c.*$', sys.argv[1])):
            if (len(sys.argv) == 3):
                complete(sys.argv[2])
            else:
                interfaz.error_value_not_defined("id")
                interfaz.limit()
                help()
        if(re.search(r'^-.*u.*$', sys.argv[1])):
            if (len(sys.argv) == 3):
                uncomplete(sys.argv[2])
            else:
                interfaz.error_value_not_defined("id")
                interfaz.limit()
                help()
    elif (sys.argv[1] == "build"):
        build()
    elif (sys.argv[1] == "all"):
        print_all()
    elif (sys.argv[1] == "new"):
        new()
    elif (sys.argv[1] == "edit"):
        if (len(sys.argv) == 3):
            todo_reto(sys.argv[2])
        else:
            interfaz.error_value_not_defined("id")
            interfaz.limit()
            help()
    elif (sys.argv[1] == "archived"):
        if (len(sys.argv) == 3):
            archived(sys.argv[2])
        else:
            interfaz.error_args("id")
    elif (sys.argv[1] == "unarchived"):
        if (len(sys.argv) == 3):
            unarchived(sys.argv[2])
        else:
            interfaz.error_args("id")
    elif (sys.argv[1] == "delete"):
        if (len(sys.argv) == 3):
            destroy(sys.argv[2])
        else:
            interfaz.error_value_not_defined("id")
            interfaz.limit()
            help()
    elif (sys.argv[1] == "todo"):
        if (len(sys.argv) == 3):
            todo_reto(sys.argv[2])
        else:
            interfaz.error_value_not_defined("id")
            interfaz.limit()
            help()
    elif (sys.argv[1] == "save"):
        if (len(sys.argv) == 3):
            save_todo(sys.argv[2])
        else:
            interfaz.error_value_not_defined("id")
            interfaz.limit()
            help()
    elif (sys.argv[1] == "modify"):
        if (len(sys.argv) == 3):
            modify(sys.argv[2])
        else:
            interfaz.error_value_not_defined("id")
            interfaz.limit()
            help()
    elif (sys.argv[1] == "destroy"):
        if (len(sys.argv) == 3):
            delete(sys.argv[2])
        else:
            interfaz.error_value_not_defined("id")
            interfaz.limit()
            help()
    elif (sys.argv[1] == "complete"):
        if (len(sys.argv) == 3):
            complete(sys.argv[2])
        else:
            interfaz.error_value_not_defined("id")
            interfaz.limit()
            help()
    elif (sys.argv[1] == "uncomplete"):
        if (len(sys.argv) == 3):
            uncomplete(sys.argv[2])
        else:
            interfaz.error_value_not_defined("id")
            interfaz.limit()
            help()
    else:
        error_args()
else:
    error_args()
