#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import getpass

class Path_db(object):
    def __init__(self):
        self.path = "/home/{}/.config/challenges/".format(getpass.getuser())

    def exist_path(self):
        return os.path.isdir(self.path)

    def create_path(self):
        os.mkdir(self.path)

    def init_path(self):
        while (not self.exist_path()):
            self.create_path()
